﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> Create(T t)
        {          
            Data = Data.Append(t);
            return Task.FromResult(t);
        }

        public Task<T> Update(T t)
        {
            T item = Data.FirstOrDefault(x => x.Id == t.Id);
            item = t;
            return Task.FromResult(t);
        }

        public void Delete(Guid id)
        {
            T t = Data.FirstOrDefault(x => x.Id == id);
            if (t != null)
            {
                Data = Data.Where(x => x.Id != id);               
            }
           
        }

    }
}